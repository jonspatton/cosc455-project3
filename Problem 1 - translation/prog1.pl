translation(ling, zero).
translation(yi, one).
translation(er, two).
translation(san, three).
translation(si, four).
translation(wu, five).
translation(liu, six).
translation(qi, seven).
translation(ba, eight).
translation(jiu, nine).
translation(shi, ten).

%base case
translate([], []).

%recursive call to a list that still has elements in it.
%This version can only translate one language at a time.
translate([Ch | Ct], [Eh | Et]) :- translation(Ch, Eh), translate(Ct, Et).
