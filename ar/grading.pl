:-dynamic(student/3).

grade(SId, Course=Grade):-retract(student(SId, Name, List)),
	assert(student(SId, Name, [ Course=Grade | List])).

grade(SId, Course+Grade):-retract(student(SId, Name, List)),
	assert(student(SId, Name, [ Course+Grade | List])).

grade(SId, Course-Grade):-retract(student(SId, Name, List)),
	assert(student(SId, Name, [ Course-Grade | List])).
