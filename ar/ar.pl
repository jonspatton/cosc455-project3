% Example of handling records: load, add, remove, save
:-dynamic(student/3).

% load the student records from a file called students.pl
load_students :- consult('students.pl'), tell('students.pl.bak'), 
	listing(student), told.

% list students
list_students :- listing(student).

% admit a student, supply SId number and Name, record with no classes
admit(SId, Name) :- assert(student(SId,Name, [])),
	write(student(SId, Name, [])), write(' added.').

% remove student record from data - supply an Id, a name or both
dismiss(SId, Name) :- retract(student(SId, Name, List)), 
	write(student(SId, Name, List)), write(' deleted.').

% save modified records in place of old ones.
save_students :- tell('students.pl'), listing(student), told.




