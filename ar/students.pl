:- dynamic student/3.

student(1234, 'Maggie Simpson', []).
student(4321, 'Lisa Simpson', [cs320=a, cs202-a, cs201+b, cs125=a]).
student(3421, 'Bart Simpson', [cs201+c]).
