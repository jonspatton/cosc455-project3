salary([jacqueline, bouvier], 23000).
salary([patty, bouvier], 23000).
salary([selma, bouvier], 23000).
salary([montgomery, burns], 1000000).
salary([larry, burns], 50000).
salary([ned, flanders], 40000).
salary([maude, flanders], 42000).
salary([rod, flanders], 0).
salary([barney, gumble], 40000).
salary([edna, krabappel], 30000).
salary([herschel, krustofski], 300000).
salary([helen, lovejoy], 30000).
salary([jessica, lovejoy], 0).
salary([timothy, lovejoy], 150000).
salary([apu, nahasapeemapetilon], 150000).
salary([jamshed, nahasapeemapetilon], 0).
salary([manjula, nahasapeemapetilon], 120000).
salary([pahusacheta, nahasapeemapetilon], 0).
salary([sanjay, nahasapeemapetilon], 0).
salary([bartholomew, simpson], 0).
salary([homer, simpson], 40000).
salary([lisa, simpson], 500).
salary([maggie, simpson], 0).
salary([marge, simpson], 10000).

familySalaries2(Name, List):- findall(Salary, salary([_, Name], Salary), List).

% ^ to prevent binding a first name for the list, so it'll make a list
% of everyone in the same family.
familySalaries(Name, List):- bagof(Salary, First^salary([First, Name], Salary), List).

%Recursive summation function.
sumOfList([],0).
sumOfList([Head|Tail], Sum):-sumOfList(Tail, S1), Sum is Head + S1.

% using Bagof -- this will give lists by family is LastName is not
% specified.
family_income(LastName, Sum):- familySalaries(LastName, List), sumOfList(List, Sum).

% Using findall -- this will give a total for the whole town if LastName
% is not specified.
family_income2(LastName, Sum):- familySalaries2(LastName, List), sumOfList(List, Sum).
