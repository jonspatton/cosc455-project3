﻿guest(mustard).
guest(plum).
guest(scarlett).
guest(green).

female(Person) :- guest(Person), \+(male(Person)).
%female(green).
%female(scarlett).

male(boddy).
male(plum).
male(mustard).

%Mr. Boddy was having an affair with Ms. Green
affair(boddy, green).

%Miss Scarlett was also having an affair with Mr. Boddy.
affair(scarlett, boddy).


%Professor Plum is married to Ms. Green.
married(plum, green).



notrich(Person) :- guest(Person), \+rich(Person).
%Mr. Boddy was very rich.
rich(boddy).

%clue that results in a unique suspect
rich(mustard).

%Colonel Mustard is very greedy.
greedy(mustard).

%The rule for who will murder over hatred.
hates(P1, Other):- (married(P1, P2); married(P2, P1)), (affair(Other, P2); affair(P2, Other)).

%The rule for who will murder over greed.
willingToMurder(P1, Victim):- notrich(P1), greedy(P1), rich(Victim).

%Find the list of people who will murder a victim.
suspect(Suspect, Victim):- willingToMurder(Suspect, Victim); hates(Suspect, Victim).








