Translation:

Run by typing
translation([number, words, in, chinese], List).
or
translation(List, [number, words, in, english]).

DigitalCircuits:
halfsubtractor(A, B, Diff, Borrow) will display the difference 
and borrow values for inputs A and B.

fullsubtractor(A, B, Cin, Diff, Borrow) will display the difference 
and borrow results for inputs A, B and C (carry in).

decoder1to2(A, In, Out1, Out2) will display the Output line results 
of a 1-to-2 line decoder circuit.

decoder2to4(A, B, In, Out0, Out1, Out2, Out3) will display the four 
Output line results of a 2-to-4 line decoder circuit.

multiplexor2(A, X0, X1, Out) will display the output of a multiplexor 
circuit given addressing unit A and inputs X0 and X1.

multiplexor4(A, B, X0, X1, X2, X3, Out) will display the output of a 
multiplexor circuit given addressing units A and B and inputs X0--X3.

flights:
Type
flight(A, B).
where A is the source city and B is the destination city. The program reports whether
a flight route exists between them.
Cities are fresno, seattle, albany, boston, dallas, omaha, atlanta

(The python version has its own Readme in the subfolder.)

Courses:
Make sure that the fall18sched.pl file is in your prolog file (or change the path
in the courses.pl file).

Note that a period is a day letter and 24-hour time, e.g. period(m, 1700)
and location is a building and room e.g. location(yr7800, 201).

Reports Who is teaching When:
is_teaching(LastName, Period).

Lists all courses in a friendly manner.
list_courses.

Lists all courses in an unfriendly manner.
listcourses.

Report whether someone teaches a course.
is_instructor(LastName, Course).

Report whether someone is busy at a time and location.
is_busy(LastName, Days, Hour, Location).

Report whether two instructors are both busy (essentially)
cannot_meet(Time, Instructor1, Instructor2).

Report whether two courses are both held at the same time.
schedule_conflict(Course1, Course2).

Add a course to the LIST. The list has to be saved for the addition to be permanent.
(Use this one).
add_course(Course, Location, Period, Instructor).

Adds a course to the LIST. The list has to be saved for the addition to be permanent.
This one takes some more explicit info and does not work as well.
add_course(Course, Building, Room, Days, Hour, Instructor).

Delete a course from the list. Must save for the change to be permanent.
delete_course(Course, Location, Period, Instructor).

Delete a course from the list. Must save for the change to be permanent.
This one takes some more explicit info and does not work as well.
delete_course(Course, Building, Room, Days, Hour, Instructor).

Save modified records in place of old ones.
save_courses.

The program will automatically load the fall18sched.pl file and 
save a backup copy (....pl.bk) upon startup, so you can roll back 
if you made a mistake but already saved.

Clue:
Simply run 
suspect(Suspect, Victim).
E.g.
suspect(Suspect, boddy)
will print out the list of suspects for Mr. Boddy's murder.

prog6 (salaries)
To get the sum of all of a family member's incomes:
family_income(LastName, Sum).
If you want a list of the salaries in a family, use:
familySalaries(Name, List).

There is aslo a family_income2(LastName, Sum). which will return the
sum of the salaries of more than one family (so you can tally the
whole town) using the list-maker familySalaries2(Name, List).

prog7: Dunk Homer

Type "homer."

The number next to each entry is the order in which the store was visited or the
gift was bought, so you just read down the columns to see what he bought where and when.

My solution was based directly on the constraints solution to the Zebra puzzle on
https://rosettacode.org/wiki/Zebra_puzzle#Prolog

I chose the constraints version because (a) the code is frankly easier to understand
for me and (b) I essentially wrote my scala solution as a constraints problem, though
it was much messier.