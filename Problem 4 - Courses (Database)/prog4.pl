:- dynamic course/4.

loadcourses(Filename, List):- see(Filename),
    inquire([],R), % gather terms from file
    reverse(R,List),
    seen.

load_courses:-consult('fall18sched.pl'), tell('fall18sched.pl.bak'), listing(course), told.


inquire(IN,OUT):- read(Data),
    (Data == end_of_file ->   % done
    OUT = IN
    ;    % more
    inquire([Data|IN],OUT) ) .

%reports Who is teaching When
is_teaching(LastName, Period):- course(_Class, _Location, Period, instructor(LastName)).

%Lists all courses in a friendly manner.
list_courses:- course(Class, location(Building, Room), time(Days, Time), instructor(LastName)), write(LastName), write(' is teaching '), write(Class), write(' in '), write(Building), write(', room '), write(Room), write(' on '), write(Days), write(' at '), write(Time), nl, fail.

%lists all courses in an unfriendly manner.
listcourses:-listing(course).

%Report whether someone teaches a course.
is_instructor(LastName, Course):- course(Course, _Location, _Period, instructor(LastName)).

%Reports whether someone is busy at a time and location.
is_busy(LastName, Days, Hour, Location):-course(_Course, Location, time(Days, Hour), instructor(LastName)).

%Reports whether two instructors are both busy (essentially)
cannot_meet(Time, Instructor1, Instructor2):- course(_Course1, _Location1, Time, instructor(Instructor1)), course(_Course2, _Location2, Time, instructor(Instructor2)).

%Reports whether two courses are both held at the same time.
schedule_conflict(Course1, Course2):- course(Course1, L1, Time, _I1), course(Course2, L1, Time, _I2).

%Adds a course to the LIST. The list has to be saved for the addition to be permanent.
%This one takes some more explicit info and does not work as well.
add_course(Course, Building, Room, Days, Hour, Instructor) :- assert(course(Course, location(Building, Room), time(Days, Hour), instructor(Instructor))),
	write(course(Course, location(Building, Room), time(Days, Hour), instructor(Instructor))), write(' added.'), nl.

%Adds a course to the LIST. The list has to be saved for the addition to be permanent.
%(Use this one).
add_course(Course, Location, Period, Instructor):- assert(course(Course, Location, Period, instructor(Instructor))), write(course(Course, Location, Period, instructor(Instructor))), write(' added.'), nl.

%Deletes a course from the list. Must save for the change to be permanent.
%This one takes some more explicit info and does not work as well.
delete_course(Course, Building, Room, Days, Hour, Instructor) :- retract(course(Course, location(Building, Room), time(Days, Hour), instructor(Instructor))),
	write(course(Course, location(Building, Room), time(Days, Hour), instructor(Instructor))), write(' deleted.'), nl.

%Deletes a course from the list. Must save for the change to be permanent.
delete_course(Course, Location, Period, Instructor) :- retract(course(Course, Location, Period, instructor(Instructor))), write(course(Course, Location, Period, instructor(Instructor))), write(' deleted.'), nl.

% save modified records in place of old ones.
save_courses :- tell('fall18sched.pl'), listing(course), told.

