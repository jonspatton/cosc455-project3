and(1, 1, 1).
and(1, 0, 0).
and(0, 1, 0).
and(0, 0, 0).

nand(1, 1, 0).
nand(1, 0, 1).
nand(0, 1, 1).
nand(0, 0, 1).

or(1, 1, 1).
or(1, 0, 1).
or(0, 1, 1).
or(0, 0, 0).

xor(0, 0, 0).
xor(0, 1, 1).
xor(1, 0, 1).
xor(1, 1, 0).

nor(0, 0, 1).
nor(0, 1, 0).
nor(1, 0, 0).
nor(1, 1, 0).

not(1, 0).
not(0, 1).

%3-input and gate (for convenience).
and3(A, B, C, Out):- and(A, B, Out1), and(Out1, C, Out).

%4-input or gate (for convenient).
or4(A, B, C, D, Out):- or(A, B, O1), or(C, D, O2), or(O1, O2, Out).

%Subtracts B from A and reports a borrow; does not accept a carry-in (well, borrow in).
halfsubtractor(A, B, Diff, Borrow):- xor(A, B, Diff), not(A, NOT1), and(B, NOT1, Borrow).

%Subtracts B from A but accepts a carry-in (well, borrow-in).
fullsubtractor(A, B, Cin, Diff, Borrow):- halfsubtractor(A, B, Dif1, Bor1), xor(Cin, Dif1, Diff), not(Dif1, NOT1), and(NOT1, Cin, BorrowIn1), or(BorrowIn1, Bor1, Borrow).

%1-to-2 line decoder. Takes state A and input In.
decoder1to2(A, In, Out1, Out2):- not(A, NOTA), and(A, In, Out1), and(NOTA, In, Out2).

%2-to-4 line decoder. Takes states A and B and a single In.
decoder2to4(A, B, In, Out0, Out1, Out2, Out3):- not(A, NOTA), not(B, NOTB), and3(NOTA, NOTB, In, Out0), and3(A, NOTB, In, Out1), and3(NOTA, B, In, Out2), and3(A, B, In, Out3).

%Convert two signals to a single out.
multiplexor2(A, X0, X1, Out):- not(A, NOTA), and(a, X1, O1), and(NOTA, X0, O2), or(O1, O2, Out).

%Convert 4 signals to a single out.
multiplexor4(A, B, X0, X1, X2, X3, Out) :- not(A, NOTA), not(B, NOTB), and3(NOTA, NOTB, X0, O0), and3(A, NOTB, X1, O1), and3(A, NOTB, X2, O2), and3(A, B, X3, O3), or4(O0, O1, O2, O3, Out).

help :- nl, write('This program generates outputs for digital circuits.'), nl, nl, write('halfsubtractor(A, B, Diff, Borrow) will display the difference and borrow values for inputs A and B.'), nl, nl,
write('fullsubtractor(A, B, Cin, Diff, Borrow) will display the difference and borrow results for inputs A, B and C (carry in).'), nl, nl,
write('decoder1to2(A, In, Out1, Out2) will display the Output line results of a 1-to-2 line decoder circuit.'), nl, nl,
write('decoder2to4(A, B, In, Out0, Out1, Out2, Out3) will display the four Output line results of a 2-to-4 line decoder circuit.'), nl, nl,
write('multiplexor2(A, X0, X1, Out) will display the output of a multiplexor circuit given addressing unit A and inputs X0 and X1.'), nl, nl,
write('multiplexor4(A, B, X0, X1, X2, X3, Out) will display the output of a multiplexor circuit given addressing units A and B and inputs X0--X3.'), nl, nl.

:- help.
