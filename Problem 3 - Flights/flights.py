import argparse

flights = [('fresno', 'seattle'),
('fresno', 'albany'),
('fresno', 'boston'),
('seattle', 'omaha'),
('seattle', 'dallas'),
('omaha', 'albany'),
('omaha', 'atlanta'),
('albany', 'seattle'),
('albany', 'dallas'),
('atlanta', 'boston'),
('atlanta', 'albany'),
('atlanta', 'dallas'),
('dallas', 'seattle'),
('dallas', 'albany')]

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("From")
    parser.add_argument("To")
    args = parser.parse_args()
    flight = (args.From, args.To)
    print(findroute(flight, flights.copy(), False))

def findroute(flight, flights, keepGoing):
    #Check if a direct route exists.
    if flight in flights:
        return True
    else:
        froms = []
        for i in range (0, len(flights)):
            #If there's a tuple with the same to and from, remove it and try again (there was a cycle)
            #This prevents infinite recursion.
            if flights[i][0] == flights[i][1]:
                return findroute(flight, flights.pop(i), False)
            #Otherwise, keep track of all the places we can get from our source.
            if flight[0] == flights[i][0]:
                froms.append(flights[i][1])
        #Then look through the edges again. If we can get to an edge[0] from one of
        #the places in the froms list, then we can get to that edge[1] from the source.
        #If a route exists, eventually this will cause there to be a direct route!
        for i in range (0, len(flights)):
            if (flights[i][0] in froms):
                flights[i] = (flight[0], flights[i][1])
                keepGoing = True
        #If we updated something, try again. If we didn't, then there's no route.
        if keepGoing:
            return findroute(flight, flights, False)
        else:
            return False
main()