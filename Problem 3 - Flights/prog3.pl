%edges
edge(fresno, seattle).
edge(fresno, albany).
edge(fresno, boston).
edge(seattle, omaha).
edge(seattle, dallas).
edge(omaha, albany).
edge(omaha, atlanta).
edge(albany, seattle).
edge(albany, dallas).
edge(atlanta, boston).
edge(atlanta, albany).
edge(atlanta, dallas).
edge(dallas, seattle).
edge(dallas, albany).

%base case: A, B share an edge.
flight(A, B):- edge(A, B).

%Recursive case: Look for a connecting edge (basic transitivity).
flight(A, B):- flight(A, C), flight(C, B).
