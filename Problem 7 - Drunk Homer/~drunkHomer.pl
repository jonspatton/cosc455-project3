gift(saxophone_book).
gift(green_dress).
gift(slingshot).
gift(pacifier).
gifts(saxophone_book, green_dress, slingshot, pacifier).

store(sprawlMart).
store(tryNSave).
store(kingToots).
store(leftorium).
stores(leftorium, sprawlMart, tryNSave, kingToots).

order(first).
order(second).
order(third).
order(fourth).
orders(first, second, third, fourth).

nextOrder(Order, Next):-orders(Order, Next, _, _); orders(_, Order, Next, _); orders(_, _, Order, Next).

previousOrder(Order, Previous):-orders(Previous, Order, _, _); orders(_, Previous, Order, _); orders(_, _, Previous, Order).

notStore(Where, NotStore):- store(Where), store(Where) \== store(NotStore).
notGift(What, NotGift):- gift(What), gift(What) \== gift(NotGift).

bought(What, Where, When):-gift(What), store(Where), order(When).

% sax book from king toots
sax(List):- findall((saxophone_book, kingToots, When), bought(saxophone_book, kingToots, When), List).

%leftorium second stop.
left(List):- findall((What, leftorium, second), bought(What, leftorium, second), List).

% The store after the slingshot was not sprawlmart.
afterSlingshot(List):- notStore(Where, sprawlMart), notGift(What, slingshot),nextOrder(_, When), findall((What, Where, When), bought(What, Where, When), List).

slingshot(List):- previousOrder(_, When), findall((slingshot, Where, When), bought(slingshot, Where, When), List).


%two stops after leaving Try-N-Save, he bought the pacifier.
try(List):- previousOrder(_, Prev), previousOrder(Prev, When), notGift(What, pacifier), findall((What, tryNSave, When), bought(What, tryNSave, When), List).
pacifier(List):- nextOrder(_, N), nextOrder(N, When), notStore(Where, tryNSave), findall((pacifier, Where, When), bought(pacifier, Where, When), List).







