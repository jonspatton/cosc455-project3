% Module documentation: http://www.swi-prolog.org/man/clpfd.html
:- use_module(library(clpfd)).

% based directly on the strategy used for the Zebra Puzzle constraint
% version at https://rosettacode.org/wiki/Zebra_puzzle#Prolog

homer :-
    Gift = [Saxophone, Slingshot, Dress, Pacifier],
    Store  = [KingToots, TryNSave, Leftorium, SprawlMart],

    % order numbers 1 to 4
    Gift ins 1..4,
    Store  ins 1..4,

    % the values in each list are exclusive
    all_different(Gift),
    all_different(Store),

    % actual constraints
    % saxophone book came from king toots
    Saxophone   #= KingToots,

    % leftorium was the second stop
    Leftorium   #= 2,

    % The store he visited immediately after buying the slingshot was not Sprawl-Mart
    Slingshot + 1 #\= SprawlMart,

    % Two stops after leaving Try-N-Save, he bought the pacifier
    TryNSave    #= Pacifier - 2,
    Pacifier    #= TryNSave + 2,

    % get solution
    flatten([Gift, Store], List), label(List),

    % print the answers
    sort([Saxophone-saxophone, Slingshot-slingshot, Dress-dress, Pacifier-pacifier],  GiftNames),
    sort([KingToots-kingToots, TryNSave-tryNSave, Leftorium-leftorium, SprawlMart-sprawlMart], StoreNames),

    Fmt = '~w~16|~w~32|~w~48|~w~n',
    format(Fmt, GiftNames),
    format(Fmt, StoreNames).
